package autoLessonLab4.stepsDefinition;

import autoLessonLab4.pagesdescription.PageStackoverflow;
import autoLessonLab4.pagesdescription.PageRozetka;
import autoLessonLab4.pagesdescription.PageStackoverflowQuestion;
import autoLessonLab4.pagesdescription.PageStackoverflowSignUp;
import autoLessonLab4.runner.TestRun;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;


public class Stepdefs {

    public static WebDriver driver = TestRun.driver;
    public static PageRozetka pageRozetka;
    String window;
    public static PageStackoverflow pageStackoverflow;
    public static PageStackoverflowSignUp pageStackoverflowSignUp;
    public static PageStackoverflowQuestion pageStackoverflowQuestion;


    @Given("^I open main page$")
    public void iOnTheMainPage() throws Throwable {
        String url = "http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
        pageRozetka = new PageRozetka(driver);
    }

    @Then("^Logo is displayed$")
    public void logoIsDisplayed() throws Throwable {
        Assert.assertTrue("Logo not displayed!", pageRozetka.rozetkaLogo.isDisplayed());
    }

    @Then("^Apple menu available in list of items$")
    public void appleMenuAvailableInListOfItems() throws Throwable {
        Assert.assertTrue("Apple menu not available in the list!",pageRozetka.categoriesApple.isDisplayed());
    }




    @When("^I click on cities link$")
    public void iClickOnCitiesLink() throws Throwable {
        window = pageRozetka.openCitySelector();
    }

    @Then("^Cities available$")
    public void citiesAvailable() throws Throwable {
        org.junit.Assert.assertTrue("Kiev not displayed!",pageRozetka.cityKiev.isDisplayed());
        org.junit.Assert.assertTrue("Kharkiv not displayed!",pageRozetka.cityKharkiv.isDisplayed());
        org.junit.Assert.assertTrue("Odessa not displayed!",pageRozetka.cityOdessa.isDisplayed());
        pageRozetka.closeCitySelector(window);
    }

    @Then("^Check MP available in submenu$")
    public void checkMPAvailableInSubmenu() throws Throwable {
        Assert.assertTrue("Sections with MP3 subsections not found!",pageRozetka.categoriesMP3.isDisplayed());
    }

    @When("^I click on trashbox$")
    public void iClickOnTrashbox() throws Throwable {
        pageRozetka.buyCart.click();
    }

    @Then("^Trashbox is empty$")
    public void trashboxIsEmpty() throws Throwable {
        org.junit.Assert.assertTrue("Not empty!",pageRozetka.cardEmpty.isDisplayed());
    }

    @Given("^I open stackoverflow main page$")
    public void iOpenStackoverflowMainPage() throws Throwable {
        String url = "http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
        pageStackoverflow = new PageStackoverflow(driver);
    }

    @Then("^Number of items in features tab is (\\d+)$")
    public void numberOfItemsInFeaturesTabIs(int arg0) throws Throwable {
        org.junit.Assert.assertTrue("Counter is lower than 300!",arg0 < Integer.parseInt(pageStackoverflow.featuresCount.getText()));
    }

    @When("^I click Sign Up link$")
    public void iClickSignUpLink() throws Throwable {
        pageStackoverflowSignUp = pageStackoverflow.sightUpClick();

    }

    @Then("^Google and Facebook available$")
    public void googleAndFacebookAvailable() throws Throwable {
        org.junit.Assert.assertTrue("Facebook or Google login not displayed!",pageStackoverflowSignUp.loginFacebook.isDisplayed() && pageStackoverflowSignUp.loginGoogle.isDisplayed());
    }

    @When("^I click on question link$")
    public void iClickOnQuestionLink() throws Throwable {
        pageStackoverflowQuestion = pageStackoverflow.topQuestionClick();
    }

    @Then("^Question was asked today$")
    public void questionWasAskedToday() throws Throwable {
        org.junit.Assert.assertTrue("Message is not created today",pageStackoverflowQuestion.checkQuestionDate());
    }

    @Then("^There is a work proposition with (\\d+) hundred$")
    public void thereIsAWorkPropositionWithHundred(int arg0) throws Throwable {
        org.junit.Assert.assertTrue("No job with $100K",pageStackoverflow.checkTextPresense(arg0));
    }
}
