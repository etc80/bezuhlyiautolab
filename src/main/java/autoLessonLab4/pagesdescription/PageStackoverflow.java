package autoLessonLab4.pagesdescription;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PageStackoverflow {

    private WebDriver driver;

    public PageStackoverflow (WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver= driver;
    }

    @FindBy(xpath="//*[@id='tabs']/a/span")
    public WebElement featuresCount;

    @FindBy(xpath = ".//*[@id='tell-me-more']")
    public WebElement sightUpLink;

    public PageStackoverflowSignUp sightUpClick() throws InterruptedException {
        sightUpLink.click();
        TimeUnit.SECONDS.sleep(5);
        return new PageStackoverflowSignUp(driver);
    }

    @FindBy(xpath="//*[@id='qlist-wrapper']/div/div[50]//a[@class='question-hyperlink']")
    public WebElement topQuestion;

    public PageStackoverflowQuestion topQuestionClick() throws InterruptedException {
        topQuestion.click();
        TimeUnit.SECONDS.sleep(5);
        return new PageStackoverflowQuestion(driver);
    }

    @FindBy(xpath = "//*[@id='hireme']//div[contains(text(),'$')]")
    public WebElement jobOffers;

    public boolean checkTextPresense(int zp){

        List<WebElement> allTitles = driver.findElements(By.xpath("//*[@id='hireme']//div[contains(text(),'$')]"));
        if (allTitles.size() >0) {
            for (WebElement w : allTitles) {
                String s = w.getText();
                String price = s.substring(s.indexOf("$"), s.length()-1);
                if (Integer.parseInt(price.toUpperCase().replace('$', ' ').replace('K', ' ').trim()) > zp)
                    return true;
            }
        }

        return false;
    }

}
