package autoLessonLab4.pagesdescription;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ai on 6/1/2016.
 */
public class PageStackoverflowSignUp {
    private WebDriver driver;

    public PageStackoverflowSignUp (WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver= driver;
    }

    @FindBy(xpath="//*[@id='openid-buttons']//span[text()='Facebook']")
    public WebElement loginFacebook;

    @FindBy(xpath = "//*[@id='openid-buttons']//span[text()='Google']")
    public WebElement loginGoogle;
}
