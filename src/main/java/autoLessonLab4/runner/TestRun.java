package autoLessonLab4.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/main/java/autoLessonLab4/features"},
        glue = {"autoLessonLab4/stepsDefinition"}
        ,tags = {}  // to run all tests
     // ,tags = { "@rozetka" }  // to run only rozetka tests
     // ,tags = {"@stackoverflow"}  // to run only stackoverflow tests
     // ,tags = {"@testrozetka1,@teststack"}  // to run only 2 steps for each feature

)

public class TestRun {
    public static WebDriver driver;

    @BeforeClass
    public static void initDriver(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }


}
