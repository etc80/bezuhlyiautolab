@stackoverflow

  Feature: test stackoverflow according to scenarios

    @teststack
    Scenario: check number of items in features tab
      Given I open stackoverflow main page
      Then Number of items in features tab is 300

    @teststack
    Scenario: check that google and facebook links available on sign up page
      Given I open stackoverflow main page
      When I click Sign Up link
      Then Google and Facebook available

    Scenario: check that question was asked today
      Given I open stackoverflow main page
      When I click on question link
      Then Question was asked today

    Scenario: check work propositions with 100K+
      Given I open stackoverflow main page
      Then There is a work proposition with 100 hundred