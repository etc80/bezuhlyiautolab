@rozetka

Feature: checking if rozetka works fine

  Background:
    Given I open main page

  @testrozetka1
  Scenario: Check Logo
    Then Logo is displayed

  @testrozetka1
  Scenario: Check Appla available in menu
    Then Apple menu available in list of items

  Scenario: Check MP3 available in submenus
    Then Check MP available in submenu

  Scenario: Check cities available to choose
    When I click on cities link
    Then Cities available

  Scenario: Check trashbox empty
    When I click on trashbox
    Then Trashbox is empty

