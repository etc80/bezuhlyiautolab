package autoLessonLab3;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class PageStackoverflowQuestion {
    private WebDriver driver;

    public PageStackoverflowQuestion (WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver= driver;
    }

    @FindBy(xpath="//*[@id='qinfo']//b[contains(text(),'today')]")
    public WebElement questionDate;

    public Boolean checkQuestionDate()
    {
        try {
            questionDate.getText().equalsIgnoreCase("today");
            return true;
        } catch (NoSuchElementException e)
        {
            System.err.println("Question not created today!");
        }
        return false;
    }

}
