package autoLessonLab3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageRozetka {

    private WebDriver driver;

    public PageRozetka(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver= driver;
    }

    @FindBy(xpath="//div[@class='logo']/img")
    public WebElement rozetkaLogo;

    @FindBy(xpath="//*[@id='m-main']/li/a[contains(text(),'Apple')]")
    public WebElement categoriesApple;

    @FindBy(xpath = "//*[@id='m-main']/li/descendant::a[contains(text(),'MP3')]")
    public WebElement categoriesMP3;

    @FindBy(xpath = ".//*[@id='city-chooser']/a/span")
    public WebElement citiesMenu;

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']//*[contains(text(),'Киев')]")
    public WebElement cityKiev;

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']//*[contains(text(),'Харьков')]")
    public WebElement cityKharkiv;

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']//*[contains(text(),'Одесса')]")
    public WebElement cityOdessa;

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/a/img")
    public WebElement closeCities;

    public String openCitySelector()
    {
        String parentHandle = driver.getWindowHandle();
        citiesMenu.click();
        for(String childHandle : driver.getWindowHandles()){
            if (!childHandle.equals(parentHandle)){
                driver.switchTo().window(childHandle);
            }
        }
        return parentHandle;
    }

    public void closeCitySelector(String parentHandle)
    {
        closeCities.click();
        driver.switchTo().window(parentHandle);
    }

    @FindBy(xpath = "//div[@name='splash-button']/descendant::span[text()='Корзина']")
    public WebElement buyCart;

    @FindBy(xpath = "//*[@id='drop-block']/h2[contains(text(),'Корзина пуста')]")
    public WebElement cardEmpty;

    @FindBy(xpath = ".//*[@id='city-chooser']//div[contains(@class,'city-choose')]")
    public WebElement cityChooser;

}
