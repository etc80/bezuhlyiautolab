package autoLessonLab8;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;


/**
 Используя инпут дату (не менее 10 вариаций инпута) из текстового документа в формате
 URL   Expected
 получить респонз и в выбранном вами теге проверить наличие Expected в нем
 Все в формате Junit тестов
 При желании можно применять сериализацию и любые доступные библиотеки по работе с апихами
 */

public class AutoLessonLab8API {

    public static boolean checkCondition (String xml, String checkConditionValue){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xmlDocument = builder.parse(new InputSource(new StringReader(xml)));
            xmlDocument.getDocumentElement().normalize();
            String expression = "pressure";
            NodeList nodeList = xmlDocument.getElementsByTagName(expression);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                Element elem = (Element)nNode;
                if (!elem.getAttribute("unit").equalsIgnoreCase(checkConditionValue))
                    return false;
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static String  readInputFromURL (String path){
        String finall="";
        try {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");


            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                finall+=output;
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return finall;
    }

    public static HashMap<String,String> readFile (String fileName){
        HashMap<String, String> dataFromFile = new HashMap<String, String>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String temp = "";

            try {
                while ((temp = reader.readLine()) != null) {
                    String[] results = parseInputDataString(temp);
                    dataFromFile.put(results[0],results[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
        e.printStackTrace();
        }
        return dataFromFile;
    }

    public static String[] parseInputDataString (String inputDataString)
    {
        String[] results = new String[2];
        results[0] = inputDataString.substring(0,inputDataString.indexOf("   "));
        results[1] = inputDataString.substring(inputDataString.lastIndexOf(' ')+1,inputDataString.length());
        return results;
    }
}