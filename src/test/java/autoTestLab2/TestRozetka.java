package autoTestLab2;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 a. Проверить наличие лого розетка в левом верхнем углу главной страницы
 b. Прорить,что меню каталога товаров содержит пункт ‘Apple’
 c. Проверить, что меню каталога товаров сождержит секцию. Содержащую ‘MP3’
 d. Кликнуть на секцию “Город” рядом с логтипом сайта, проверить что там присутствуют линки на города “Харьков”,, Одесса”, “Киев”
 e. Перейти в Корзину и проверить что она пуста
 */
public class TestRozetka {

    private static WebDriver driver;

    @BeforeClass
    public static void initDriver(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }

    @Before
    public void openMainPage(){
        String url = "http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Test
    public void testA(){
        Assert.assertTrue("Logo not found",driver.findElement(By.xpath("//div[@class='logo']/img")).isDisplayed());
    }

    @Test
    public void testB(){
        Assert.assertTrue("Apple not in menu!",driver.findElement(By.xpath("//*[@id='m-main']/li/a[contains(text(),'Apple')]")).isDisplayed());
    }

    @Test
    public void testC(){
        Assert.assertTrue("Sections with MP3 subsections not found!",driver.findElement(By.xpath("//*[@id='m-main']/li/descendant::a[contains(text(),'MP3')]")).isDisplayed());
    }

    @Test
    public void testD() throws  Exception{
        driver.findElement(By.xpath(".//*[@id='city-chooser']/a/span")).click();
        Assert.assertTrue("Киев not displayed!",driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(text(),'Киев')]")).isDisplayed());
        Assert.assertTrue("Харьков not displayed!",driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(text(),'Харьков')]")).isDisplayed());
        Assert.assertTrue("Одесса not displayed!",driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(text(),'Одесса')]")).isDisplayed());
        driver.findElement(By.xpath(".//*[@id='city-chooser']/div[1]/div/a/img")).click();
    }

    @Test
    public void testE(){
        driver.findElement(By.xpath("//div[@name='splash-button']/descendant::span[text()='Корзина']")).click();
        Assert.assertTrue("Not empty!",driver.findElement(By.xpath(".//*[@id='drop-block']/h2[contains(text(),'Корзина пуста')]")).isDisplayed());
    }
}
