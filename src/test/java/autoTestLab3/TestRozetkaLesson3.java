package autoTestLab3;

import autoLessonLab3.PageRozetka;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 a. Проверить наличие лого розетка в левом верхнем углу главной страницы
 b. Прорить,что меню каталога товаров содержит пункт ‘Apple’
 c. Проверить, что меню каталога товаров сождержит секцию. Содержащую ‘MP3’
 d. Кликнуть на секцию “Город” рядом с логтипом сайта, проверить что там присутствуют линки на города “Харьков”,, Одесса”, “Киев”
 e. Перейти в Корзину и проверить что она пуста
 */
public class TestRozetkaLesson3 {

    private static WebDriver driver;
    protected PageRozetka pageRozetka;

    @BeforeClass
    public static void initDriver(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }

    @Before
    public void openMainPage(){
        String url = "http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
        pageRozetka = new PageRozetka(driver);
    }

    @Test
    public void testA(){
        Assert.assertTrue("logo not found!",pageRozetka.rozetkaLogo.isDisplayed());
    }

    @Test
    public void testB(){
        Assert.assertTrue("Apple not in menu!",pageRozetka.categoriesApple.isDisplayed());
    }

    @Test
    public void testC(){
        Assert.assertTrue("Sections with MP3 subsections not found!",pageRozetka.categoriesMP3.isDisplayed());
    }

    @Test
    public void testD() throws  Exception{
        String window = pageRozetka.openCitySelector();
        Assert.assertTrue("Kiev not displayed!",pageRozetka.cityKiev.isDisplayed());
        Assert.assertTrue("Kharkiv not displayed!",pageRozetka.cityKharkiv.isDisplayed());
        Assert.assertTrue("Odessa not displayed!",pageRozetka.cityOdessa.isDisplayed());
        pageRozetka.closeCitySelector(window);
    }

    @Test
    public void testE(){
        pageRozetka.buyCart.click();
        Assert.assertTrue("Not empty!",pageRozetka.cardEmpty.isDisplayed());
    }
}
