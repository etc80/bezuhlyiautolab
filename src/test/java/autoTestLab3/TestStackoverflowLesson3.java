package autoTestLab3;

import autoLessonLab3.PageStackoverflow;
import autoLessonLab3.PageStackoverflowQuestion;
import autoLessonLab3.PageStackoverflowSignUp;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/****
 A. Проверить, что количество в табе ‘featured’ главного блока сайта больше 300
 B. Кликнуть на главной странице Sing Up, проверить что на открывшейся странице присутствуют кнопки для входа из социальных сетей - Google, Facebook
 C. Кликнуть на главной странице любой вопрос из секции Top Questions и на открывшейся странице проверить, что вопрос был задан сегодня.
 D. Дополнительно: проверить, что на главной странице stackoverflow есть предложение о работе с зарплатой больше $ 100k :)
 (тест должен упасть, если такого предложения нет).
 */
public class TestStackoverflowLesson3 {

    private static WebDriver driver;
    protected PageStackoverflow pageStackoverflow;
    protected PageStackoverflowSignUp pageStackoverflowSignUp;
    protected PageStackoverflowQuestion pageStackoverflowQuestion;

    @BeforeClass
    public static void initDriver(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }

    @Before
    public void openMainPage(){
        String url = "http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
        pageStackoverflow = new PageStackoverflow(driver);
    }

    @Test
    public void testA(){
        Assert.assertTrue("Counter is lower than 300!",300 < Integer.parseInt(pageStackoverflow.featuresCount.getText()));
    }

    @Test
    public void testB() throws Exception{
        pageStackoverflowSignUp = pageStackoverflow.sightUpClick();
        Assert.assertTrue("Facebook or Google login not displayed!",pageStackoverflowSignUp.loginFacebook.isDisplayed() && pageStackoverflowSignUp.loginGoogle.isDisplayed());
    }

    @Test
    public void testC()throws Exception {

        pageStackoverflowQuestion = pageStackoverflow.topQuestionClick();
        Assert.assertTrue("Message is not created today",pageStackoverflowQuestion.checkQuestionDate());
    }

    @Test
    public void testD()throws Exception{

        Assert.assertTrue("No job with $100K",pageStackoverflow.checkTextPresense());
    }

}
