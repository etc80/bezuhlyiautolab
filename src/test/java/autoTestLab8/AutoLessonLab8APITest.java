package autoTestLab8;


import org.junit.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static autoLessonLab8.AutoLessonLab8API.*;



public class AutoLessonLab8APITest {

    @Test
    public void testAPI() throws IOException {

        HashMap<String,String> data = readFile("src/URLwithAPIs");

        FileWriter writer = new FileWriter("src/URLwithAPIs", false);

        for (Map.Entry entry : data.entrySet()) {

            boolean result = checkCondition(readInputFromURL(entry.getKey().toString()), entry.getValue().toString());;
            try {
                System.out.println(result);
                Assert.assertTrue(result);
            } catch (AssertionError e)
            {
                System.err.println(result);
            }
            String text = entry.getKey().toString()+"   "+entry.getValue().toString()+"   " + result;
            writer.write(text);
            writer.append('\n');
        }
        writer.flush();
    }
}
